import UIKit
import Flutter
import GetStream

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    //seersite-dev
    let appId: String = "67084";
    let apiKey: String = "qsew4du86xvt";
    var feed: FlatFeed?
    
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let channel = FlutterMethodChannel(name: "io.getstream/backend",
                                           binaryMessenger: controller.binaryMessenger)
        
        channel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            let args = call.arguments as!  Dictionary<String, String>
            if call.method == "getHomeFeed" {
                do {
                    self!.getHomeFeed(args: args, result: result)
                } catch let error {
                    result(FlutterError.init(code: "IOS_EXCEPTION_getTimeline",
                                             message: error.localizedDescription,
                                             details: nil))
                }
            }  else {
                result(FlutterError.init(code: "IOS_EXCEPTION_NO_METHOD_FOUND",
                                         message: "no method found for: " + call.method,
                                         details: nil));
            }
        })
        
        GeneratedPluginRegistrant.register(with: self)
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    private func getHomeFeed(args: Dictionary<String, String>, result: @escaping FlutterResult) {
        let client = Client(apiKey: apiKey, appId: appId, token: args["token"]!, logsEnabled: true)
        self.feed = client.flatFeed(feedSlug: "user", userId: args["userId"]!)
        
        self.feed!.get(typeOf: PostActivity.self, enrich: true, pagination: .limit(25)) { r in
            result(String(data: try! JSONEncoder().encode(try! r.get().results), encoding: .utf8)!)
        }
    }
}

final class UserActivity: CustomStringConvertible, Encodable {
    
    var name: String?
    var description: String {
        return "name=\(String(describing: name))"
    }
    
    init (container: KeyedDecodingContainer<PostActivity.CodingKeys.UserContent.DataContent>) throws{
        name = try container.decodeIfPresent(String.self, forKey: .name)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PostActivity.CodingKeys.UserContent.DataContent.self)
        try container.encode(name, forKey: .name)
    }
    
}

final class ContentActivity: CustomStringConvertible, Encodable {
    
    var title: String?
    var body: String?
    var url: String?
    var attachment: String?
    
    var description: String{
        return "title=\(String(describing: title)), body=\(String(describing: body)), url=\(String(describing: url))"
    }
    
    init (container: KeyedDecodingContainer<PostActivity.CodingKeys.PostContent>) throws{
        title = try container.decodeIfPresent(String.self, forKey: .title)
        body = try container.decodeIfPresent(String.self, forKey: .body)
        if (container.contains(.attachment)){
            let attachmentContainer = try container.nestedContainer(keyedBy: PostActivity.CodingKeys.PostContent.PostAttachment.self, forKey: .attachment)
            url = try attachmentContainer.decodeIfPresent(String.self, forKey: .url)
        }
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: PostActivity.CodingKeys.PostContent.self)
        try container.encode(title, forKey: .title)
        try container.encode(body, forKey: .body)
        try container.encode(url, forKey: .url)
    }
}


final class PostActivity: EnrichedActivity<User, String, DefaultReaction> {
    
    var content: ContentActivity?
    var user: UserActivity?
    
    enum CodingKeys: String, CodingKey {
        case content
        case actor
        case user
        
        enum PostContent: String, CodingKey {
            case title = "title"
            case body = "body"
            case url = "url"
            case attachment = "attachment"

            enum PostAttachment: String, CodingKey {
                case url = "url"
            }
        }
        
        enum UserContent: String, CodingKey {
            case data = "data"
            
            enum DataContent: String, CodingKey {
                case name = "name"
            }
        }
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let postContainer = try container.nestedContainer(keyedBy: CodingKeys.PostContent.self, forKey: .content)
        let userContainer = try container.nestedContainer(keyedBy: CodingKeys.UserContent.self, forKey: .actor)
        let dataContainer = try userContainer.nestedContainer(keyedBy: CodingKeys.UserContent.DataContent.self, forKey: .data)
        self.content = try ContentActivity(container: postContainer)
        self.user = try UserActivity(container: dataContainer)
        try super.init(from: decoder)
    }
    
    required init(actor: ActorType, verb: Verb, object: ObjectType, foreignId: String? = nil, time: Date? = nil, feedIds: FeedIds? = nil, originFeedId: FeedId? = nil) {
        fatalError("init(actor:verb:object:foreignId:time:feedIds:originFeedId:) has not been implemented")
    }
    
    override public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(content, forKey: .content)
        try container.encode(user, forKey: .user)
        try super.encode(to: encoder)
    }
}


