import 'package:flutter/material.dart';

import 'api_service.dart';

class Home extends StatefulWidget {
  Home({Key key, @required this.account}) : super(key: key);
  final Map account;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  Future<List<dynamic>> _activities;

  @override
  void initState() {
    super.initState();
    _activities = _getHomeFeed();
  }

  Future<List<dynamic>> _getHomeFeed() async {
    return await ApiService().getHomeFeed(widget.account);
  }

  Future _refreshActivities() async {
    setState(() {
      _activities = _getHomeFeed();
    });
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<dynamic>>(
      future: _activities,
      builder: (BuildContext context, AsyncSnapshot<List<dynamic>> snapshot) {
        if (!snapshot.hasData) {
          return Center(child: CircularProgressIndicator());
        }
        return Container(
          child: Center(
            child: RefreshIndicator(
              onRefresh: _refreshActivities,
              child: ListView(
                children: snapshot.data
                    .map((activity) =>
                    Container(
                      padding: EdgeInsets.all(20.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Column(),
                              Expanded(
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Text(activity["user"] != null ? activity["user"]["name"] : activity["actor"]["name"])
                                      ],
                                    ),

                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          child: Container(
                                            padding: EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
                                            child: Text(
                                                activity['content'] != null &&
                                                    activity['content']
                                                    ['title'] !=
                                                        null
                                                    ? activity['content']['title']
                                                    : '', softWrap: true,
                                                maxLines: 2,
                                                overflow: TextOverflow.ellipsis,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.bold)),),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )

                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Expanded(
                                child: Container(
                                  padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 0.0),
                                  child: Text(
                                      activity['content'] != null
                                          ? activity['content']['body']
                                          : 'test',
                                      softWrap: true,
                                      maxLines: 5,
                                      overflow: TextOverflow.ellipsis),
                                ),
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Visibility(
                                visible: activity["content"]["attachment"]!=null?true:activity["content"]["url"]!=null?true:true,
                                child: Expanded(
                                  child: Image.network(activity["content"]["attachment"]!=null?activity["content"]["attachment"]["url"]:activity["content"]["url"]!=null?activity["content"]["url"]:'https://picsum.photos/250?image=9', width: 600, height: 240, fit: BoxFit.fitWidth),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    ))
                    .toList(),
              ),
            ),
          ),
        );
      },
    );
  }
}
