import 'package:flutter/material.dart';
import 'api_service.dart';
import 'home.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Test App',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: MyHomePage(title: 'seersite'),
    debugShowCheckedModeBanner: false);
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  final _email = TextEditingController();
  final _password = TextEditingController();

  Map<String, String> _account;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Future _login(BuildContext context) async {
    if (_email.text.length > 0) {
      var creds = await ApiService().login(_email.text, _password.text);
      setState(() {
        _account = {
          'userEmail': _email.text,
          'authToken': creds['authToken'],
          'streamToken': creds['streamToken'],
          'userId': creds['userId']
        };
      });
    } else {
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content: Text('Invalid User'),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    if (_account != null) {
      var body;
      if (_selectedIndex == 0) {
        body = Home(account: _account);
      } else if (_selectedIndex == 1) {
        //body = Users();
      }

      return Scaffold(
          appBar: AppBar(
            title: Text(widget.title),
          ),
          body: body,
          bottomNavigationBar: BottomNavigationBar(
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                  icon: Icon(Icons.home), title: Text('Home')),
              BottomNavigationBarItem(
                  icon: Icon(Icons.people), title: Text('Users')),
            ],
            currentIndex: _selectedIndex,
            onTap: _onItemTapped,
          ));
    } else {
      return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Builder(
          builder: (BuildContext context) {
            return Container(
              padding: EdgeInsets.all(50.0),
              child: Center(
                child: Column(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), hintText: 'email'),
                        controller: _email,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: TextField(
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), hintText: 'password'),
                        controller: _password,
                        obscureText: true,
                      ),
                    ),
                    RaisedButton(
                      onPressed: () => _login(context),
                      child: Text("Login"),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      );
    }
  }
}
