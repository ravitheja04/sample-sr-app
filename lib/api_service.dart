import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'environments/.env' as env;

class ApiService{

  static const platform = const MethodChannel('io.getstream/backend');

  var baseUrl = env.BASE_URL;

  Future<Map> login(String email, String password) async {
    final headers = {"Accept": "application/json", "Content-Type": "application/x-www-form-urlencoded"};
    String formBody =  'email=' + Uri.encodeQueryComponent(email) + '&password=' + Uri.encodeQueryComponent(password);
    var authResponse = await http.post('$baseUrl/login-user', headers: headers, body: formBody);
    var jsonResponse = json.decode(authResponse.body);
    var userId = jsonResponse['data']['_id'];
    var authToken = jsonResponse['data']['token'];
    var streamToken = jsonResponse['data']['streamConfig']['streamToken'];
    print("$baseUrl : output url");
    return {'authToken': authToken, 'streamToken': streamToken, 'userId': userId};
  }

  Future<dynamic> getHomeFeed(Map account) async {
    print ('API SERVICE-GET HOME FEED');
    print(account['userId']);
    var result =
    await platform.invokeMethod<String>('getHomeFeed', {'userId': account['userId'], 'token': account['streamToken']});
    return json.decode(result);
  }

}