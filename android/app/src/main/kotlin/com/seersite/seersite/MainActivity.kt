package com.seersite.seersite
import io.getstream.cloud.CloudClient
import com.fasterxml.jackson.databind.ObjectMapper
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine

import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import io.getstream.core.options.Limit
import io.getstream.core.models.EnrichedActivity


class MainActivity: FlutterActivity() {

    private val CHANNEL = "io.getstream/backend"
    private val API_KEY = "qsew4du86xvt"
    private val APP_ID = "67084"

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine)
        MethodChannel(flutterEngine.dartExecutor, CHANNEL).setMethodCallHandler { call, result ->
            if (call.method == "getHomeFeed") {
                val activities = getHomeFeed(
                        call.argument<String>("userId")!!,
                        call.argument<String>("token")!!
                )
                result.success(ObjectMapper().writeValueAsString(activities))
            }
        }
    }

    private fun getHomeFeed(userId: String, token: String): MutableList<EnrichedActivity>? {
        val client = CloudClient.builder(API_KEY, token, userId).build()
        return client.flatFeed("user").getEnrichedActivities(Limit(25)).join()
    }

}
